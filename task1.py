# -*-coding:utf-8-*-
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import OneHotEncoder
from sklearn.naive_bayes import MultinomialNB
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import GridSearchCV
from sklearn import metrics
from sklearn.metrics import accuracy_score, make_scorer
import numpy as np
##
def made_data(doc1,doc2):
    with open(doc1, 'r') as train_file , open(doc2, 'r') as test_file:
        lines1 = train_file.readlines()  #读取训练数据
        lines2 = test_file.readlines()  # 读取测试数据
        con_feature = []  # 离散特征
        dis_feature = []  # 连续特征
        line_label = []   # 类别
        # print(len(lines1),len(lines2))
        train_num=len(lines1)
        for line in lines1+lines2:    # 训练数据和测试数据放在一块处理
            line = line.strip().replace('.','').split(', ')
            if len(line) != 15 or '?' in line:    # 筛除异常数据
                continue

            item_con_feature = [line[0], line[2], line[4], line[10], line[11], line[12]]  # 连续特征
            con_feature.append(item_con_feature)

            item_dis_feature = [line[1], line[3], line[5], line[6], line[7], line[8], line[9], line[13]]  #离散特征
            dis_feature.append(item_dis_feature)

            line_label.append(line[14])   # 类别

        con_feature_array = np.array(con_feature)  # 列表转数组

        min_max_scaler = MinMaxScaler()
        con_feature_minmax = min_max_scaler.fit_transform(con_feature_array)  # 连续数据归一化

    for col in range(0,len(line_label)):    # 类别标签转为0-1值
        if '<=' in line_label[col]:
            line_label[col]=1
        else:
            line_label[col]=0
    le = LabelEncoder()                  # 对离散型特征编码
    for col in range(len(dis_feature)):
        dis_feature[col] = le.fit_transform(dis_feature[col])

    enc = OneHotEncoder()  # 生成独热向量
    enc.fit(dis_feature)
    b_dis = enc.transform(dis_feature)

    data1 = con_feature_minmax
    data2 = b_dis.toarray()
    data = np.hstack((data1, data2))
    return data[:train_num],line_label[:train_num],data[train_num:],line_label[train_num:]


if __name__ == '__main__':
    doc1=r'file\\adult.data'
    doc2=r'file\\adult.test'
    train_data,train_label,test_data,test_label=made_data(doc1,doc2)   ##数据预处理

    clf = RandomForestClassifier()     ##选择随机森林


    acc_scorer = make_scorer(accuracy_score)
    parameters = {'n_estimators': [5, 10, 15],
                  'criterion': ['entropy', 'gini']
                  }
    grid_obj = GridSearchCV(clf, parameters, scoring=acc_scorer)
    grid_obj = grid_obj.fit(train_data, train_label)
    clf = grid_obj.best_estimator_

    clf = clf.fit(train_data, train_label)
    test_predictions = clf.predict(test_data)
    print("accuracy：",accuracy_score(test_label, test_predictions))
    f1_=metrics.f1_score(test_label, test_predictions, average='weighted')  # 验证集上的auc值
    print("f1 score: %s" %f1_)

    test_auc = metrics.roc_auc_score(test_label, test_predictions)  # 验证集上的auc值
    print("auc score: %s" % test_auc)

