from keras.preprocessing.text import Tokenizer
import numpy as np
from file.auc import auc
from file.f1 import f1
import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation
from sklearn.model_selection import train_test_split
import os

max_words = 1000  #最常出现的单词前多少个
batch_size = 10   #每批次的数量
epochs = 10   #轮数
path = r'file\\20_newsgroups\\'
texts=[]
labels=[]


##################读取文档####################################
dirs = os.listdir(path)
# print(dirs)
for tem in range(len(dirs)):
    files = os.listdir(path+dirs[tem])
    for file in files:
        with open(path+dirs[tem]+r'\\'+file, 'r') as text_file:
            texts.append(text_file.read().replace(r"\n", ""))
            labels.append(tem)
print(len(texts))


####################数据预处理########### #########
tokenizer = Tokenizer(num_words=1000)
tokenizer.fit_on_texts(texts)
a=tokenizer.texts_to_matrix(texts,r'tfidf')
x_train, x_test, y_train, y_test = train_test_split(a, labels, test_size = 0.2)

print(len(x_train), 'train len')
print(len(x_test), 'test len')

num_classes = np.max(y_train) + 1
print(num_classes, 'classes')
print('x_train shape:', x_train.shape)
print('x_test shape:', x_test.shape)

print('Convert class vector to binary class matrix '
      '(for use with categorical_crossentropy)')
#分类转成独热向量
y_train = keras.utils.to_categorical(y_train, num_classes)
y_test = keras.utils.to_categorical(y_test, num_classes)
print('y_train shape:', y_train.shape)
print('y_test shape:', y_test.shape)


################模型###################################
model = Sequential()
model.add(Dense(512, input_shape=(max_words,)))
model.add(Activation('relu'))
model.add(Dropout(0.5))

model.add(Dense(num_classes))
model.add(Activation('softmax'))
model.summary()

################评估##################################
##由于keras没有自带 f1和AUC, 需要借助外部文件：f1.py  auc.py
model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy',f1,auc])

history = model.fit(x_train, y_train, batch_size=batch_size, epochs=epochs, verbose=1, validation_split=0.1)
score = model.evaluate(x_test, y_test, batch_size=batch_size, verbose=1)
print('acc:', score[1])
print('f1 score:', score[2])
print('auc score:', score[3])